//
//  AddIssueViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/2/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class AddIssueViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var bodyText: UITextField!
    
    var issue: NSManagedObject?
    var repo: NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleText.delegate = self
        bodyText.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func changeStatusButton(_ sender: Any) {
        guard (bodyText.text != "") && (titleText.text != "") else { return }
        let repository = (repo?.value(forKeyPath: "name") as? String)!
        let params = [
            "title": titleText.text,
            "body": bodyText.text
        ]
        
        APIManager.sharedInstance.createIssue(repository: repository, params: params as Any as! [String : Any], complition: {
            (error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                CoreDataManager.sharedInstance.saveIssue(title: self.titleText.text!, body: self.bodyText.text!, to: self.repo as! Repository)
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
}

