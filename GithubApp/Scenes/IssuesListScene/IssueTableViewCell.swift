//
//  IssueTableViewCell.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit

class IssueTableViewCell: UITableViewCell {

    @IBOutlet weak var issueTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
