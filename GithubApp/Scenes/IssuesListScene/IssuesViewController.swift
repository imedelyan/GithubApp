//
//  IssuesViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 3/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class IssuesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var repo: NSManagedObject?
    var issues: [NSManagedObject] = []
    var viewModel: IssuesViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Issues"
        
        getIssues()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getIssues() {
        
        issues = CoreDataManager.sharedInstance.getIssuesForRepo(self.repo as! Repository)!
        guard (issues == []) else {
            tableView.reloadData()
            return
        }
        
        let repository = (repo?.value(forKeyPath: "name") as? String)!
        
        APIManager.sharedInstance.getRepositoryIssues(repository: repository, complition: {
            (issues, error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                self.issues = issues!
                //print("Issues: \(self.issues)")
                self.tableView.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IssueDetailedSegue" {
            if let nextViewController = segue.destination as? IssueViewController{
                if let indexPath = sender as? IndexPath {
                    nextViewController.issue = issues[indexPath.row]
                    nextViewController.repo = repo // TODO: get it from DB for Comments request
                }
            }
        }
        if segue.identifier == "AddIssueSegue" {
            if let nextViewController = segue.destination as? AddIssueViewController{
                nextViewController.repo = repo // TODO: get it from DB
            }
        }
    }
}

extension IssuesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.issues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IssueCell", for: indexPath) as! IssueTableViewCell
        let issue = issues[indexPath.row]
        cell.issueTitle?.text = issue.value(forKeyPath: "title") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "IssueDetailedSegue", sender: indexPath)
    }
    
}
