//
//  AddCommentViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/2/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class AddCommentViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var bodyText: UITextField!
    
    var issue: NSManagedObject?
    var repo: NSManagedObject? // TODO: get it from DB
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bodyText.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func changeStatusButton(_ sender: Any) {
        guard (bodyText.text != "") else { return }
        let repository = (repo?.value(forKeyPath: "name") as? String)!
        let iss = (self.issue?.value(forKeyPath: "number") as? Int)!
        
        APIManager.sharedInstance.createComment(repository: repository, issueNumber: iss, comment: bodyText.text!, complition: {
            (error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                CoreDataManager.sharedInstance.saveComment(self.bodyText.text!, to: self.issue as! Issue)
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
}
