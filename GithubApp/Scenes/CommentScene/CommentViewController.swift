//
//  CommentViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/2/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class CommentViewController: UIViewController {

    @IBOutlet weak var bodyText: UITextView!
    var comment: NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Comment"
        bodyText.text = comment?.value(forKeyPath: "body") as? String
    }
}
