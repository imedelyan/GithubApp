//
//  CommentsViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class CommentsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var issue: NSManagedObject?
    var comments: [NSManagedObject] = []
    var repo: NSManagedObject? // TODO: get it from DB
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Comments"
        
        getComments()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func getComments() {
        comments = CoreDataManager.sharedInstance.getCommentsForIssue(self.issue as! Issue)!
        guard (comments == []) else {
            tableView.reloadData()
            return
        }
        
        let repository = (repo?.value(forKeyPath: "name") as? String)!
        let issue = (self.issue?.value(forKeyPath: "number") as? Int)!
        
        APIManager.sharedInstance.getComments(repository: repository, issueNumber: issue, complition: {
            (comments, error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                self.comments = comments!
                //print("Comments: \(self.comments)")
                self.tableView.reloadData()
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentSegue" {
            if let nextViewController = segue.destination as? CommentViewController{
                if let indexPath = sender as? IndexPath {
                    nextViewController.comment = comments[indexPath.row]
                }
            }
        }
        if segue.identifier == "AddCommentSegue" {
            if let nextViewController = segue.destination as? AddCommentViewController{
                nextViewController.issue = issue
                nextViewController.repo = repo // TODO: get it from DB for Comments request
            }
        }
    }
}

extension CommentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentTableViewCell
        let comment = comments[indexPath.row]
        cell.commentBody?.text = comment.value(forKeyPath: "body") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "CommentSegue", sender: indexPath)
    }
}
