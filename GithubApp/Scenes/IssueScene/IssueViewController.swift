//
//  IssueViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

public enum IssueState: String {
    case open
    case closed
}

class IssueViewController: UIViewController {

    var issue: NSManagedObject?
    var repo: NSManagedObject? // TODO: get it from DB for Comments request
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyText: UITextView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Issue"
        titleLabel.text = issue?.value(forKeyPath: "title") as? String
        bodyText.text = issue?.value(forKeyPath: "body") as? String
        statusLabel.text = issue?.value(forKeyPath: "state") as? String
    }
    
    func changeStatusText(_ status: String) -> String {
        switch status {
        case "open":
            return "close"
        case "close":
            return "open"
        default:
            break
        }
        return ""
    }
    
    @IBAction func changeStatusButton(_ sender: Any) {
        let repository = (repo?.value(forKeyPath: "name") as? String)!
        let iss = (self.issue?.value(forKeyPath: "number") as? Int)!
        let params = ["state": changeStatusText(statusLabel.text!)]
        
        APIManager.sharedInstance.updateIssue(repository: repository, issueNumber: iss, params: params, complition: {
            (error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                let status = self.changeStatusText(self.statusLabel.text!)
                CoreDataManager.sharedInstance.updateIssue(number: iss, newStatus: status)
                self.statusLabel.text = status
            }
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentsSegue" {
            if let nextViewController = segue.destination as? CommentsViewController{
                nextViewController.issue = issue
                nextViewController.repo = repo // TODO: get it from DB for Comments request
            }
        }
    }
}
