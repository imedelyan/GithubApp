//
//  RepositoryTableViewCell.swift
//  GithubApp
//
//  Created by Igor Medelyan on 3/31/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var repositoryName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
