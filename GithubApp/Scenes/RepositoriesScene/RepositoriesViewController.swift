//
//  RepositoriesViewController.swift
//  GithubApp
//
//  Created by Igor Medelyan on 3/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import CoreData

class RepositoriesViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    var repos: [NSManagedObject] = []
    var viewModel: RepositoriesViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Repositories"
        
        if (!UserDefaults.standard.bool(forKey: "loadingOAuthToken")) {
            loadInitialData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func loadInitialData() {
        if (!AuthManager.sharedInstance.hasOAuthToken()) {
            AuthManager.sharedInstance.OAuthTokenCompletionHandler = {
                (error) -> Void in
                if let receivedError = error {
                    print(receivedError)
                    // TODO: handle error
                    AuthManager.sharedInstance.startOAuth2Login()
                } else {
                    self.getRepositories()
                }
            }
            AuthManager.sharedInstance.startOAuth2Login()
        } else {
            getRepositories()
        }
    }
    
    func getRepositories() {
        
        repos = CoreDataManager.sharedInstance.getRepositories()!
        guard (repos == []) else {
            tableView.reloadData()
            return
        }
        
        APIManager.sharedInstance.getRepositories(complition: { (repos, error) in
            if let receivedError = error {
                print(receivedError)
                // TODO: display error
            } else {
                self.repos = repos!
                //print("Repositories: \(self.repos)")
                self.tableView.reloadData()
            }
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IssuesSegue" {
            if let nextViewController = segue.destination as? IssuesViewController{
                if let indexPath = sender as? IndexPath {
                    nextViewController.repo = repos[indexPath.row]
                }
            }
        }
    }
}

extension RepositoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoriesCell", for: indexPath) as! RepositoryTableViewCell
        let repo = repos[indexPath.row]
        cell.repositoryName?.text = repo.value(forKeyPath: "name") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "IssuesSegue", sender: indexPath)
    }

}
