//
//  ScenesCoordinator.swift
//  GithubApp
//
//  Created by Igor Medelyan on 3/29/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import UIKit
import Foundation

class ScenesCoordinator {
    var window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func isLoggedIn() -> Bool {
        return true
    }
    
    func start() {
        if isLoggedIn() {
            showRepositories()
        } else {
            showAuthentication()
        }
    }
}

extension ScenesCoordinator {
    func showAuthentication() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "Login") as? RepositoriesViewController
        window.rootViewController = loginViewController
    }

    func showRepositories() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let repositoriesViewController = storyboard.instantiateViewController(withIdentifier: "Repositories") as? RepositoriesViewController
        let navigationController = UINavigationController(rootViewController: repositoriesViewController!)
        navigationController.navigationBar.backgroundColor = UIColor.blue
        window.rootViewController = navigationController
    }
    
   /* func pushScene(viewModel: Any) {
        window.rootViewController = UINavigationController()
        switch viewModel {
        case is LoginViewModel.Type:
            let viewController = LoginViewController()
            window.rootViewController?.show(viewController, sender: nil)
        case is RepositoriesViewModel.Type:
            let viewController = RepositoriesViewController()
            window.rootViewController?.show(viewController, sender: nil)
        default:
            print("Some other type")
        }
    }*/
}
