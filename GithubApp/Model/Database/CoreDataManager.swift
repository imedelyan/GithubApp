//
//  CoreDataManager.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    //TODO: divide CoreDataManager to different classes, rename functions, make them universal
    
    static let sharedInstance = CoreDataManager()
    
    private let appDelegate: AppDelegate
    private let context: NSManagedObjectContext
    let entities = ["Repository", "Issue", "Comment"]
    
    init() {
        self.appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = self.appDelegate.persistentContainer.viewContext
    }

    func clearDB() {
        for i in 0...2 {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entities[i])
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try context.execute(batchDeleteRequest)
            } catch {
                // Error Handling
            }
        }
    }
    
    func save() {
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func getRepositoriesArray(from jsonArray: Any) -> [NSManagedObject]? {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var repos: [NSManagedObject] = []
        
        for jsonObject in jsonArray {
            let entity = NSEntityDescription.entity(forEntityName: "Repository", in: context)!
            let repo = NSManagedObject(entity: entity, insertInto: context)
            
            repo.setValue(jsonObject["description"] as? String, forKeyPath: "desc")
            repo.setValue(jsonObject["name"] as? String, forKeyPath: "name")
            let owner = jsonObject["owner"] as? [String: Any]
            repo.setValue(owner!["login"] as? String, forKeyPath: "ownerLogin")
            repo.setValue(jsonObject["url"] as? String, forKeyPath: "url")
            
            APIManager.sharedInstance.owner = owner!["login"] as? String ?? ""
            save()
            repos.append(repo)
        }
        return repos
    }
    
    func getIssuesArray(from jsonArray: Any) -> [NSManagedObject]? {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var issues: [NSManagedObject] = []
        
        for jsonObject in jsonArray {
            let entity = NSEntityDescription.entity(forEntityName: "Issue", in: context)!
            let issue = NSManagedObject(entity: entity, insertInto: context)
            
            issue.setValue(jsonObject["number"] as? Int, forKeyPath: "number")
            issue.setValue(jsonObject["state"] as? String, forKeyPath: "state")
            issue.setValue(jsonObject["title"] as? String, forKeyPath: "title")
            issue.setValue(jsonObject["body"] as? String, forKeyPath: "body")
            let user = jsonObject["user"] as? [String: Any]
            issue.setValue(user!["login"] as? String, forKeyPath: "userLogin")
            issue.setValue(jsonObject["comments"] as? Int, forKeyPath: "comments")
            issue.setValue(jsonObject["created_at"] as? String, forKeyPath: "created_at")
            issue.setValue(jsonObject["updated_at"] as? String, forKeyPath: "updated_at")
            issue.setValue(jsonObject["url"] as? String, forKeyPath: "url")
            
            save()
            issues.append(issue)
        }
        return issues
    }

    func getCommentsArray(from jsonArray: Any) -> [NSManagedObject]? {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var comments: [NSManagedObject] = []
        
        for jsonObject in jsonArray {
            let entity = NSEntityDescription.entity(forEntityName: "Comment", in: context)!
            let comment = NSManagedObject(entity: entity, insertInto: context)
            
            comment.setValue(jsonObject["body"] as? String, forKeyPath: "body")
            let user = jsonObject["user"] as? [String: Any]
            comment.setValue(user!["login"] as? String, forKeyPath: "userLogin")
            comment.setValue(jsonObject["created_at"] as? String, forKeyPath: "created_at")
            comment.setValue(jsonObject["updated_at"] as? String, forKeyPath: "updated_at")
            comment.setValue(jsonObject["url"] as? String, forKeyPath: "url")
            
            save()
            comments.append(comment)
        }
        return comments
    }
    
    func saveComment(_ comment: String, to issue: Issue) {
        let entity = NSEntityDescription.entity(forEntityName: "Comment", in: context)!
        let comm = NSManagedObject(entity: entity, insertInto: context)
        
        //TODO: optimize initialization
        comm.setValue(comment, forKeyPath: "body")
        comm.setValue("", forKeyPath: "created_at")
        comm.setValue("", forKeyPath: "updated_at")
        comm.setValue("", forKeyPath: "url")
        comm.setValue("", forKeyPath: "userLogin")
        //comm.setValue(issue, forKeyPath: "issue") //needs to be updated
        
        save()
    }
    
    func getCommentsForIssue(_ issue: Issue) -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Comment")
        fetchRequest.predicate = NSPredicate(format: "%K == %@", "issue", issue)
        var comments: [NSManagedObject] = []
    
        do {
            comments = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return comments
    }
    
    func saveIssue(title: String, body: String, to repo: Repository) {
        let entity = NSEntityDescription.entity(forEntityName: "Issue", in: context)!
        let iss = NSManagedObject(entity: entity, insertInto: context)
        
        //TODO: optimize initialization
        iss.setValue(body, forKeyPath: "body")
        iss.setValue(title, forKeyPath: "title")
        iss.setValue("open", forKeyPath: "state")
        //iss.setValue(repo, forKeyPath: "repository") //needs to be updated
        
        save()
    }
    
    func updateIssue(number: Int, newStatus: String) { //TODO: predicate doesn't work, nothing is saved - make it work
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Issue")
        fetchRequest.predicate = NSPredicate(format: "number == %@", String(number))
        
        do {
            let issues: [NSManagedObject] = try context.fetch(fetchRequest)
            if (issues.count > 0) {
                let issue = issues[0] as! Issue
                issue.setValue(newStatus, forKeyPath: "state")
                save()
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func getIssuesForRepo(_ repo: Repository) -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Issue")
        fetchRequest.predicate = NSPredicate(format: "%K == %@", "repository", repo)
        var issues: [NSManagedObject] = []
        
        do {
            issues = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return issues
    }

    func getRepositories() -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Repository")
        var repos: [NSManagedObject] = []
        
        do {
            repos = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return repos
    }
}
