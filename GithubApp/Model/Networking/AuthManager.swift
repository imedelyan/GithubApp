//
//  Authentication.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

class AuthManager {
    
    static let sharedInstance = AuthManager()
    
    //TODO: Save credentials in safe way
    var clientID: String = "c68b448ccc15ab78debb"
    var clientSecret: String = "5d06c0ead67c73e58a1dd0494e1c23a8ef8ad2b2"
    
    let baseUrl = "https://github.com/login/oauth"
    let noOAuthError = NSError(domain: NSURLErrorDomain, code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not obtain an OAuth token", NSLocalizedRecoverySuggestionErrorKey: "Please retry your request"])
    
    var OAuthTokenCompletionHandler:((NSError?) -> Void)?
    
    var OAuthToken: String? {
        set {
            if let valueToSave = newValue { KeychainWrapper.standard.set(valueToSave, forKey: "token") }
            else { KeychainWrapper.standard.removeObject(forKey: "token") }
        }
        get {
            if let token = KeychainWrapper.standard.string(forKey: "token") { return token }
            return nil
        }
    }
    
    func hasOAuthToken() -> Bool {
        if let token = self.OAuthToken {
            return !token.isEmpty
        }
        return false
    }
    
    func processOAuthResponse(url: NSURL) {
        let components = NSURLComponents(url: url as URL, resolvingAgainstBaseURL: false)
        var code: String?
        if let queryItems = components?.queryItems {
            for queryItem in queryItems {
                if (queryItem.name.lowercased() == "code") {
                    code = queryItem.value
                    break
                }
            }
        }
        if let receivedCode = code {
            let getTokenPath = baseUrl + "/access_token"
            let tokenParams = ["client_id": clientID, "client_secret": clientSecret, "code": receivedCode]
            
            request(getTokenPath, method: .post, parameters: tokenParams)
                .responseString { response in
                    switch response.result {
                    case .success(let receivedResults):

                        //TODO: optimize getting token string from code
                        let resultParams: [Substring] = receivedResults.split(separator: "&")
                        for param in resultParams {
                            let resultsSplit = param.split(separator: "=")
                            if (resultsSplit.count == 2) {
                                let key = String(resultsSplit[0]).lowercased() // access_token, scope, token_type
                                let value = String(resultsSplit[1])
                                switch key {
                                case "access_token":
                                    self.OAuthToken = value
                                case "scope":
                                    print("SET SCOPE") // TODO: verify scope
                                case "token_type":
                                    print("CHECK IF BEARER") // TODO: verify bearer
                                default:
                                    print("Some other type of key in response string for Token")
                                }
                            }
                        }
                        UserDefaults.standard.set(false, forKey: "loadingOAuthToken")
                        if let completionHandler = self.OAuthTokenCompletionHandler {
                            if self.hasOAuthToken() {
                                completionHandler(nil)
                            } else {
                                completionHandler(self.noOAuthError)
                            }
                        }
                    case .failure(let error):
                        print(error)
                        UserDefaults.standard.set(false, forKey: "loadingOAuthToken")
                        if let completionHandler = self.OAuthTokenCompletionHandler {
                            completionHandler(self.noOAuthError)
                        }
                        //return
                    }
            }
        }
    }
    
    func startOAuth2Login() {
        let authPath = baseUrl + "/authorize?client_id=\(clientID)&scope=repo&state=TEST_STATE"
        if let authURL = NSURL(string: authPath) {
            UserDefaults.standard.set(true, forKey: "loadingOAuthToken")
            UIApplication.shared.open(authURL as URL, options: [:], completionHandler: nil)
        }
    }
}
