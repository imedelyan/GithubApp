//
//  CommentsAPI.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

extension APIManager {
    
    //List comments on an issue:
    //GET /repos/:owner/:repo/issues/:number/comments
    func getComments(repository: String, issueNumber: Int, complition: @escaping ([NSManagedObject]?, NSError?) -> Void) {
        let path = baseUrl + "/repos/\(self.owner)/\(repository)/issues/\(issueNumber)/comments"
        
        manager.request(path, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success(let value):
                    guard let comments = CoreDataManager.sharedInstance.getCommentsArray(from: value) else { return }
                    complition(comments, nil)
                case .failure(let error):
                    complition(nil, error as NSError)
                }
        }
    }
    
    //Create a comment
    //POST /repos/:owner/:repo/issues/:number/comments
    //{"body": "Me too"}
    func createComment(repository: String, issueNumber: Int, comment: String, complition: @escaping(Error?) -> Void) {
        let path = baseUrl + "/repos/\(self.owner)/\(repository)/issues/\(issueNumber)/comments"
        let params = ["body": comment]
        
        manager.request(path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success:
                    complition(nil)
                case .failure(let error):
                    complition(error as NSError)
                }
        }
    }
    
    
    //TODO: Implement other functions
    
    //Edit a comment
    //PATCH /repos/:owner/:repo/issues/comments/:id
    //{"body": "Me too"}
    
    //Delete a comment
    //DELETE /repos/:owner/:repo/issues/comments/:id
    
    //List comments in a repository
    //GET /repos/:owner/:repo/issues/comments
    
    //Get a single comment
    //GET /repos/:owner/:repo/issues/comments/:id
}
