//
//  IssuesAPI.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

extension APIManager {
    
    public enum IssueState: String {
        case open
        case closed
    }
    
    //List all issues for a repository:
    func getRepositoryIssues(repository: String, complition: @escaping ([NSManagedObject]?, NSError?) -> Void) {
        let path = baseUrl + "/repos/\(self.owner)/\(repository)/issues"
        
        manager.request(path, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                //print(responseJSON)
                switch responseJSON.result {
                case .success(let value):
                    guard let issues = CoreDataManager.sharedInstance.getIssuesArray(from: value) else { return }
                    complition(issues, nil)
                case .failure(let error):
                    complition(nil, error as NSError)
                }
        }
    }
    
    //Create issue for repository for Owner:
    /*{
    "title": "Found a bug",
    "body": "I'm having a problem with this.",
    "assignees": [
    "octocat"
    ],
    "milestone": 1,
    "labels": [
    "bug"
    ]
    }*/
    func createIssue(repository: String, params: [String: Any], complition: @escaping(Error?) -> Void) {
        let path = baseUrl + "/repos/\(self.owner)/\(repository)/issues"
        
        manager.request(path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success:
                    complition(nil)
                case .failure(let error):
                    complition(error as NSError)
                }
        }
    }
    
    //Update issue for repository for Owner by number:
    func updateIssue(repository: String, issueNumber: Int, params: [String: Any], complition: @escaping(Error?) -> Void) {
        let path = baseUrl + "/repos/\(self.owner)/\(repository)/issues/\(issueNumber)"
        
        manager.request(path, method: .patch, parameters: params, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success:
                    complition(nil)
                case .failure(let error):
                    complition(error as NSError)
                }
        }
    }
    
    //TODO: Implement other functions
    
    //Get issue for repository for Owner by issue number:
    //func getIssue(owner: String, repository: String, number: Int, complition: @escaping (Issue?, NSError?) -> Void) {
        //let path = baseUrl + "/repos/\(owner)/\(repository)/issues/\(number)"
    //}
    
    //List all issues assigned to the authenticated user across all visible repositories including owned repositories, member repositories, and organization repositories:
    //func getIssues() { }
    
    //List all issues across owned and member repositories assigned to the authenticated user:
    //func getUserIssues() { }
    
    //List all issues for a given organization assigned to the authenticated user:
    //func getIssuesForOrganization() { }
}
