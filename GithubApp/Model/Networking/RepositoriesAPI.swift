//
//  RepositoriesAPI.swift
//  GithubApp
//
//  Created by Igor Medelyan on 4/1/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

extension APIManager {
    
    //List all repositories for authenticated user:
    func getRepositories(complition: @escaping ([NSManagedObject]?, NSError?) -> Void) {
        let path = baseUrl + "/user/repos"
        
        manager.request(path, encoding: JSONEncoding.default, headers: tokenHeader)
            .validate()
            .responseJSON { responseJSON in
                switch responseJSON.result {
                case .success(let value):
                    guard let repos = CoreDataManager.sharedInstance.getRepositoriesArray(from: value) else { return }
                    complition(repos, nil)
                case .failure(let error):
                    complition(nil, error as NSError)
                }
        }
    }
    
    //TODO: Implement other functions
    
    //List all repositories for User:
    //func getRepositoriesForUser() { }
    
    //List all repositories for Owner:
    //func getRepositoriesForOwner() { }
    
    //List all repositories for Organization:
    //func getRepositoriesForOrganization() { }
}
