//
//  APIManager.swift
//  GithubApp
//
//  Created by Igor Medelyan on 3/28/18.
//  Copyright © 2018 Igor Medelyan. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    
    static let sharedInstance = APIManager()
    var manager: SessionManager = SessionManager.default
    
    let baseUrl = "https://api.github.com"
    var tokenHeader: [String: String] { return ["Authorization": "token \(AuthManager.sharedInstance.OAuthToken!)"] }
    var owner: String = ""
    
    //TODO: make universal functions GET POST PATCH
}
